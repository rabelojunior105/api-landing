import Sequelize, { Model } from 'sequelize';
import bcrypt from 'bcryptjs';

class User extends Model {
  static init(sequelize) {
    super.init(
      {
        /* Name,company_name,tellphone, cnpj,email, password */

        name: Sequelize.STRING,
        company_name: Sequelize.STRING,
        tellphone: Sequelize.STRING,
        cnpj: Sequelize.STRING,
        email: Sequelize.STRING,
        password: Sequelize.VIRTUAL,
        password_hash: Sequelize.STRING,
      },
      {
        sequelize,
      }
    );
    this.addHook('beforeSave', async user => {
      if (user.password) {
        user.password_hash = await bcrypt.hash(user.password, 8);
      }
    });
    return this;
  }

  /*   static associate(models) {
    this.belongsTo(models.File, {
      foreignKey: 'fk_avatar',
      as: 'avatar',
    });
  }
 */
  checkPassword(password) {
    return bcrypt.compare(password, this.password_hash);
  }
}

export default User;
