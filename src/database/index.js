import Sequelize from 'sequelize';

// Import Mongo DB for creating notifications
/* import mongoose from 'mongoose'; */

import db from '../config/database';

import User from '../app/models/Users';

const models = [User];

class Database {
  constructor() {
    this.init();
    /* this.mongo(); */
  }

  init() {
    this.connection = new Sequelize(db);

    models
      .map(model => model.init(this.connection))
      .map(model => model.associate && model.associate(this.connection.models));
  }

  /*   mongo() {
    this.mongoConnection = mongoose.connect(process.env.MONGO_URL, {
      useNewUrlParser: true,
      useFindAndModify: true,
      useUnifiedTopology: true,
    });
  } */
}

export default new Database();
