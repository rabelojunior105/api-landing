import { Router } from 'express';

import UserController from './app/controllers/UserController';

const routes = new Router();
// Session e cadastro dos Usuarios

routes.get('/', (req, res) => {
  return res.send('this is connection success');
});

routes.post('/users', UserController.store);
// Middware para testar se o usuario está com token

export default routes;
